<section class="swiper swiper_banners">
  <div class="swiper-wrapper">
    <div class="swiper-slide shadow">
      <img class="hidden lg:block w-full" src="<?php echo $URI->base("/admin/uploads/banners/banerdesktop.png"); ?>" />
      <img class="lg:hidden block w-full" src="<?php echo $URI->base("/admin/uploads/banners/bannermobile.png"); ?>" />
    </div>
  </div>
</section>