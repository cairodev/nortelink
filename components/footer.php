<footer class="bg-color3 mt-10 border border-gray-300">
  <div class="max-w-6xl p-4 py-6 mx-auto lg:py-16 md:p-8 lg:p-10">
    <div class="grid grid-cols-2">
      <div>
        <img class="w-56" src="<?php echo $URI->base('/assets/img/logo.png') ?>">
        <i class="bi bi-whatsapp"></i>
        <i class="bi bi-instagram"></i>
        <i class="bi bi-facebook"></i>
      </div>
      <div>
        <h3 class="mb-6 text-sm font-semibold text-color2 uppercase">Contato</h3>
        <p class="text-color2">
          R. Senador Cândido Ferraz, 1250 Sala 604 - Jóquei, Teresina - PI
          Cep:64.049-250
        </p>
      </div>
    </div>
    <div class="text-center pt-10">
      <span class="block text-base text-center text-color2">© 2023 - NORTELINK.COM.BR
      </span>
      <div class="pt-10">
        <span class="block text-xs text-center text-color2">Site criado por
        </span>
        <span class="block text-xs text-center text-color2">Web Developer Full Stack: @cairofelipedev
        </span>
      </div>
    </div>
  </div>
</footer>

<script src="<?php echo $URI->base('/assets/js/dark_mode.js') ?>"></script>
<script src="https://unpkg.com/flowbite@1.4.1/dist/flowbite.js"></script>