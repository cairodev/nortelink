<div class="mx-auto max-w-6xl">
  <section class="swiper swiper_products products pt-5 lg:p-0 p-2" id="planos">
    <h1 class="lg:text-5xl text-3xl text-center mb-4"><span class="font-semibold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color2"> SinCom</span></h1>
    <h1 class="lg:text-3xl text-2xl mt-5 mb-4"><span class="font-black"> Um software para organizar seu comércio!</span></h1>

    <h1 class="text-justify">Com uma interface simples e intuitiva, o SINCOM disponibiliza aos seus usuários uma série de recursos que facilitam as operações do dia a dia na empresa, como o registro de compras e vendas de produtos e serviços, a negociação com clientes, a emissão de documentos fiscais, o fechamento de caixa, o planejamento financeiro, dentre outras. Além disso, o sistema permite o controle inteligente do andamento do negócio, gerando consultas e relatórios analíticos mais precisos, que mostram a realidade atual da empresa e ajudam na tomada de decisões, buscando sempre os melhores resultados.</h1>

  </section>
</div>
<section class="pt-10">
  <img class="hidden lg:block w-full" src="<?php echo $URI->base("/admin/uploads/banners/banner4.jpg"); ?>" />
  <img class="lg:hidden block w-full" src="<?php echo $URI->base("/admin/uploads/banners/banner5.jpg"); ?>" />
</section>