<?php
$URI = new URI();
?>
<title>Norte Link - iCompany RT</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="iCompany RT, Rt, Norte, Link, Sistemas, Gestão Comercial, Varejo, Atacado, Comercial, Sistema para Varejo, Sistema Comercial" name="keywords">
<meta content="GESTÃO COMERCIAL INTEGRADA PARA VAREJO E ATACADO" name="description">
<meta name="author" content="Cairo Felipe Developer">

<meta property="og:title" content="Norte Link - iCompany RT" />
<meta property="og:url" content="icompanyrt.nortelink.com.br" />
<meta property="og:image" content="<?php echo $URI->base("/assets/img/logo"); ?>" />
<meta property="og:description" content="<?php echo $description; ?>" />

<link rel="stylesheet" href="<?php echo $URI->base('/assets/css/style.css') ?>">
<link href="<?php echo $URI->base("/assets/img/icon.png"); ?>" rel="icon">
<link href="<?php echo $URI->base("/assets/img/icon.png"); ?>" rel="apple-touch-icon">
<script src="https://cdn.tailwindcss.com"></script>
<script>
	tailwind.config = {
		theme: {
			extend: {
				colors: {
					color1: '#26A44E',
					color2: '#2C284B',
					color3: '#FFFFFF',
				}
			}
		}
	}
</script>